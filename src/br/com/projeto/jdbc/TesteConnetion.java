package br.com.projeto.jdbc;

import javax.swing.JOptionPane;

/**
 *
 * @author Rodrigo Miranda
 */
public class TesteConnetion {
    public static void main(String[] args) {
        
        try {
            
            new ConnectionFactory().getConnection();
            JOptionPane.showMessageDialog(null, "Conectado com sucesso!");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Conexão mal sucedida. . : \n " + e);
        }
    }
    
}
