package br.com.projeto.jdbc;

/**
 *
 * @author Rodrigo Miranda
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ConnectionFactory {

    //conecta com banco de dados
    public Connection getConnection() {

        Connection conexao = null;
        try {
            conexao = DriverManager.getConnection("jdbc:mysql://localhost:3306/bdvendas", "x", "x");
            //JOptionPane.showMessageDialog(null, "Conexao bem sucedida.");
            //System.out.println("Conexao bem sucedida.");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Conexao mau sucedida.");
            System.out.print("\n Erro ao conectar: " + e.toString());
        }
        return (conexao);

    }

}
